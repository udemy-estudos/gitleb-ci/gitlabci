---
id: cursos-ti
title: Cursos de TI
sidebar_label: TI
---

## Cursos de TI por segmentos

`#NossosCursos`

### DevOps
![](/website/static/img/optydevio-logo.ico)

Na Ciência da Computação o DevOps (contração de development e operations), é uma cultura na engenharia de software que aproxima os **desenvolvedores de software (Dev) e os operadores do software / administradores do sistema (Ops)**, com característica principal de melhorar a comunicação dos dois papéis dentro de um projeto e defender a automação e monitoramento em todas as fases da construção de um software (desde a integração, teste, liberação para implantação, ao gerenciamento de infraestrutura), auxiliam empresas no gerenciamento de lançamento de novas versões, padronizando ambientes em ciclos de desenvolvimento menores, frequência de implantação aumentada, liberações mais seguras, em alinhamento próximo com os objetivos de negócio.

[- DevOps]()
[- FullStacks]()
[- FullCycle]()

### Arquiteturas
A arquitetura de software de um sistema consiste na definição dos componentes de software, suas propriedades externas, e seus relacionamentos com outros softwares. O termo também se refere à documentação da arquitetura de software do sistema. A documentação da arquitetura do software facilita: a comunicação entre os stakeholders, registra as decisões iniciais acerca do projeto de alto-nível, e permite o reúso do projeto dos componentes e padrões entre projetos.

[- Microservices (Microserviços)]()
[- Aplicação Monolitica]()
[- MVC (Model View Controler)]()
[- Destribuida]()
[- Client Server (Cliente Servidor)]()
[- Service-Oriented Architecture (SOA)]()

### Infraestutura
A infraestrutura de TI consiste nos componentes e serviços que fornecem a base para sustentar todos os sistemas de informação de uma organização. Ter uma organização competitiva exige uma grande variedade de equipamentos, softwares e ferramentas de comunicação para funcionar e fazer o básico, além dos serviços de um profissional treinado que saiba operar e administrar toda essa tecnologia. 

Os componentes (também conhecidos como plataforma) são divididos em hardware, software e tecnologias de comunicação e providenciam a base de todo o sistema. O profissional de TI usa estes componentes para gerar serviços, que incluem gerenciamento de dados, desenvolvimento de sistemas e novos métodos de segurança.

**Hardware:** Consiste na tecnologia para processamento computacional, armazenamento, entrada e saída de dados. Inclui, também, equipamentos para reunir e registrar dados, meios físicos para armazená-los e os dispositivos de saída da informação processada.
Software: É dividido em softwares de sistema e de aplicativos. Os de sistema administram os recursos e as atividades do computador. Os de aplicativos direcionam o computador a uma tarefa específica solicitada pelo usuário.
**Rede:** Proporciona conectividade de dados entre funcionários, clientes e fornecedores. Isso inclui a tecnologia para operar as redes internas da organização, os serviços prestados por companhias telefônicas ou de telecomunicações e a tecnologia para operar sites e conectar-se com outros sistemas computacionais por meio da internet.
**Serviços:** As organizações precisam de pessoas para operar e gerenciar os outros componentes da infraestrutura de TI, além de ensinar seus funcionários a usar essas tecnologias em suas atividades. Nem mesmo as grandes organizações tem a equipe, o orçamento ou a experiência requerida para implantar e operar a ampla gama de tecnologias que necessitam. Quando precisam fazer alterações profundas em seus sistemas, ou implantar uma estrutura completamente nova, em geral, as organizações recorrem a consultores externos que as ajudam com a integração do sistema. Existem milhares de fornecedores de tecnologias que oferecem serviços e componentes da infraestrutura de TI, assim como um número igualmente grande de maneiras de combiná-los.[1]

[- Ansible](iaas-ansible.md)
[- Terraform](iaas-terraform.md)
[- Serverless Frameworks](iaas-serverless-frameworks.md)
[- Docker]()
[- Kubernetes ***(k8s)***]()

### Desenvolvedor
#### Linguagens
[- golang]()
[- c#]()
[- Typescript]()
[- PHP]()
[- JS]()

#### Mobile
[- Flutter]()
[- React]()

#### Frontends Desktop
[- DotNet]()
[- Vue]()
[- JS]()
[- Electron]()

#### Frontends Web
[- DotNet]()
[- Angular]()
[- React]()

#### Backends
[- Nodejs]()
[- SpringBoot]()
[- DotNet]()

### CI/CD
[- Jenkins]()
[- Trevis]()
[- GitLabCI]()

### Monitoramento
[- Grafana]()
[- Prometheus]()
[- Istio]()

### Elastic Stacks
[- Search]() 
[- Kibana]()
[- Beats]()
[- Logstash]()